# Kisphp Faker

[![pipeline status](https://gitlab.com/kisphp/faker/badges/master/pipeline.svg)](https://gitlab.com/kisphp/faker/-/commits/master)
[![coverage report](https://gitlab.com/kisphp/faker/badges/master/coverage.svg)](https://gitlab.com/kisphp/faker/-/commits/master)

> This is the core library of **kisphp/faker**. Please install **kisphp/faker-{locale}** instead

## Installation

```bash
composer require kisphp/faker-en
```

## Usage

```php
<?php

require '/path/to/vendor/autoload.php';

$faker = \Kisphp\Faker\Factory::create();

// get a masculine name
echo $faker->person->firstNameMale;

// get a feminine name
echo $faker->person->firstNameFemale;

// get a random paragraph (generated from LoremIpsum)
echo $faker->text->randomParagraph();
```
