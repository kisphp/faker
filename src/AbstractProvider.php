<?php

namespace Kisphp\Faker;

abstract class AbstractProvider
{
    public function __get($name)
    {
        if (property_exists($this, $name)) {
            return $this->getRandomValue($this->$name);
        }

        $method = 'get' . ucfirst($name);
        if (method_exists($this, $method)) {
            return $this->$method();
        }

        throw new \Exception('Property ' . $name . ' not found');
    }

    /**
     * @param $propertyName
     * @return string
     */
    public function getProperty($propertyName)
    {
        if (property_exists($this, $propertyName)) {
            return $this->$propertyName;
        }

        throw new \Exception('Property ' . $propertyName . ' not found in provider ' . get_called_class());
    }

    public function setProperty($propertyName, $value)
    {
        $this->$propertyName = $value;
    }

    /**
     * @param array $data
     * @return string
     */
    public function getRandomValue(array $data)
    {
        shuffle($data);
        $this->seed();
        $random = array_rand($data);

        return $data[$random];
    }

    /**
     * @param int $seed
     */
    protected function seed(int $seed = null)
    {
        mt_srand();
    }
}
