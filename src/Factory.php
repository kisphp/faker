<?php

namespace Kisphp\Faker;

abstract class Factory
{
    const DEFAULT_LOCALE = 'en';

    /**
     * @param string $locale
     * @return KisphpGenerator
     */
    public static function create($locale = self::DEFAULT_LOCALE)
    {
        return new KisphpGenerator($locale);
    }
}
