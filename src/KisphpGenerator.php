<?php

namespace Kisphp\Faker;

use Kisphp\Faker\Type\Address;
use Kisphp\Faker\Type\Country;
use Kisphp\Faker\Type\Gps;
use Kisphp\Faker\Type\Person;
use Kisphp\Faker\Type\Text;

/**
 * @property Person $person
 * @property Text $text
 */
class KisphpGenerator
{
    /**
     * @var Person
     */
    protected $person;

    /**
     * @var Text
     */
    protected $text;

    /**
     * @var Address
     */
    protected $address;

    /**
     * @var Gps
     */
    protected $gps;

    /**
     * @var Country
     */
    protected $country;

    /**
     * @param string $locale
     * @throws \Exception
     */
    public function __construct($locale = 'en')
    {
        $localeNamespace = $this->localeToNamespace($locale);

        $this->person = new Person($this->loadProvider('Person', $localeNamespace));
        $this->text = new Text($this->loadProvider('Text', $localeNamespace));
        $this->address = new Address($this->loadProvider('Address', $localeNamespace));
        $this->gps = new Gps($this->loadProvider('Gps', $localeNamespace));
        $this->country = new Country($this->loadProvider('Country', $localeNamespace));
    }

    /**
     * @param $name
     * @return mixed
     * @throws \Exception
     */
    public function __get($name)
    {
        if (property_exists($this, $name)) {
            return $this->$name;
        }

        throw new \Exception('Property ' . $name . ' not found');
    }

    /**
     * @param int $number
     * @return string
     */
    public function text($number=100)
    {
        return $this->text->text($number);
    }

    /**
     * @return string
     */
    public function country()
    {
        return $this->country->country();
    }

    /**
     * @param string $className
     * @param string $localeNamespace
     * @return AbstractProvider
     * @throws \Exception
     */
    protected function loadProvider($className, $localeNamespace)
    {
        $providerClass = 'Kisphp\Faker\\'.$localeNamespace.'\Provider\\' . $className;
        if (class_exists($providerClass)) {
            return new $providerClass();
        }

        $defaultProviderClass = 'Kisphp\Faker\Provider\\' . $className . 'Provider';
        if (class_exists($defaultProviderClass)) {
            return new $defaultProviderClass();
        }

        throw new \Exception('Provider not found: ' . $providerClass);
    }

    /**
     * @param string $locale
     * @return string
     */
    protected function localeToNamespace($locale)
    {
        $locale = str_replace(['-', '_'], ' ', strtolower($locale));
        $locale = ucwords($locale);

        return preg_replace('/([\s]+)/', '', $locale);
    }
}
