<?php

namespace Kisphp\Faker\Type;

use Kisphp\Faker\AbstractType;

class Text extends AbstractType
{
    const TEXT_MAX_LENGTH = 10000;

    /**
     * @var string
     */
    protected $storyContent;

    /**
     * @var string[]
     */
    protected $storyParagraphs = [];

    /**
     * @return false|string
     */
    protected function getStoryContent()
    {
        if ($this->storyContent === null) {
            $this->storyContent = file_get_contents(__DIR__ . '/../../content/story.txt');

            $paragraphs = explode("\n", $this->storyContent);

            $this->storyParagraphs = array_filter($paragraphs, function($value){
                return !empty($value);
            });
        }

        return $this->storyContent;
    }

    /**
     * @param int $characters
     * @return string
     */
    public function text(int $characters = 100)
    {
        $characters = min(static::TEXT_MAX_LENGTH, max(0, $characters));

        $content = '';
        do {
            $content .= $this->randomParagraph();
        } while (strlen($content) <= $characters);

        return substr($content, 0, $characters);
    }

    /**
     * @return string
     */
    public function randomParagraph()
    {
        $this->getStoryContent();

        shuffle($this->storyParagraphs);
        $random = array_rand($this->storyParagraphs);

        return $this->storyParagraphs[$random];
    }
}
