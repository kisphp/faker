<?php


namespace Kisphp\Faker\Type;


use Kisphp\Faker\AbstractType;

class Country extends AbstractType
{
    public function country()
    {
        return $this->country;
    }
}