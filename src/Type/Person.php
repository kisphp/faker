<?php

namespace Kisphp\Faker\Type;

use Kisphp\Faker\AbstractType;

/**
 * @property string $firstNameMale
 * @property string $firstNameFemale
 * @property string $lastName
 * @property string $prefix
 * @property string $suffix
 * @property string $name
 * @property string $age
 */
class Person extends AbstractType
{
    const GENDER_MALE = 'male';
    const GENDER_FEMALE = 'female';

    protected $fistName;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    /**
     * @param string $gender
     * @return string
     */
    public function getFirstName($gender=null)
    {
        if ($gender === self::GENDER_MALE) {
            return $this->provider->firstNameMale;
        }
        if ($gender === self::GENDER_FEMALE) {
            return $this->provider->firstNameFemale;
        }

        $this->provider->setProperty('firstName', array_merge(
            $this->provider->getProperty('firstNameMale'),
            $this->provider->getProperty('firstNameFemale')
        ));

        return $this->provider->getRandomValue($this->firstName);
    }
}
