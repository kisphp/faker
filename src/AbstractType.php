<?php

namespace Kisphp\Faker;

abstract class AbstractType
{
    /**
     * @var AbstractProvider
     */
    protected $provider;

    /**
     * @param AbstractProvider $provider
     */
    public function __construct(AbstractProvider $provider)
    {
        $this->provider = $provider;
    }

    /**
     * @param string $name
     * @return string
     * @throws \Exception
     */
    public function __get($name)
    {
        $methodName = 'get' . ucwords($name);
        if (method_exists($this, $methodName)) {
            return $this->$methodName();
        }

        return $this->provider->$name;
    }
}
