.PONY: i t it

COMPOSER = $(shell which composer)
PHPUNIT = vendor/bin/phpunit
PHP = $(shell which php)

t:
	$(PHPUNIT)

i:
	$(COMPOSER) install -o -a
