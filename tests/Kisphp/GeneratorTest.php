<?php

namespace Tests\Kisphp;

use Kisphp\Faker\Factory;
use PHPUnit\Framework\TestCase;

class GeneratorTest extends TestCase
{
    public function testPersonProvider()
    {
        $generator = Factory::create('test');

        $this->assertSame('Smith', $generator->person->lastName);
        $this->assertSame('John', $generator->person->firstNameMale);
        $this->assertSame('John', $generator->person->getFirstName('male'));
        $this->assertSame('Alice', $generator->person->getFirstName('female'));
        $this->assertStringContainsString('Smith', $generator->person->getName());
    }

    public function testPropertyNotFound()
    {
        $generator = Factory::create('test');

        $this->expectException(\Exception::class);
        $generator->missing;
    }

    public function testProviderPropertyNotFound()
    {
        $generator = Factory::create('test');

        $this->expectException(\Exception::class);
        $generator->person->missing;
    }

    public function testMissingProvider()
    {
        $this->expectException(\Exception::class);

        Factory::create('missing');
    }
}
