<?php

namespace Tests\Kisphp;

use Kisphp\Faker\Factory;
use PHPUnit\Framework\TestCase;

class TextTypeTest extends TestCase
{
    public function testTextReturn()
    {
        $generator = Factory::create('test');

        $this->assertEquals(300 , strlen($generator->text(300)));
    }

    public function testHugeTextReturn()
    {
        $generator = Factory::create('test');

        $this->assertEquals(10000 , strlen($generator->text(20000)));
        $this->assertEquals(8000 , strlen($generator->text(8000)));
    }

    public function testNoTextReturn()
    {
        $generator = Factory::create('test');

        $this->assertEquals(0 , strlen($generator->text(0)));
    }

    public function testTextNegativeReturn()
    {
        $generator = Factory::create('test');

        $this->assertEquals(0 , strlen($generator->text(-10)));
    }

    public function testParagraph()
    {
        $generator = Factory::create('test');

        $this->assertGreaterThan(100 , strlen($generator->text->randomParagraph()));
    }

    public function testSameSubstringNotMatch()
    {
        $generator = Factory::create('test');

        $this->assertNotSame(
            $generator->text(30),
            $generator->text(30)
        );
    }
}
