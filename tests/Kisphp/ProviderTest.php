<?php

namespace Tests\Kisphp;

use Kisphp\Faker\Test\Provider\Person;
use PHPUnit\Framework\TestCase;

class ProviderTest extends TestCase
{
    public function testPersonProvider()
    {
        $provider = new Person();

        $this->assertIsArray($provider->getProperty('firstNameMale'));
    }

    public function testPersonMissingProvider()
    {
        $provider = new Person();

        $this->expectException(\Exception::class);
        $provider->getProperty('missing');
    }

}
