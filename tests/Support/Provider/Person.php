<?php

namespace Kisphp\Faker\Test\Provider;

use Kisphp\Faker\AbstractProvider;

class Person extends AbstractProvider
{
    protected $firstNameMale = ['John'];
    protected $firstNameFemale = ['Alice'];
    protected $lastName = ['Smith'];
    protected $prefixMale = ['Mr.'];
    protected $prefixFemale = ['Ms.'];
    protected $suffix = ['Phd.', 'Dr.'];
    protected $age = [];
}
